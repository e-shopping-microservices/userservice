
create table users(
    id int auto_increment,
    name varchar(20),
    balance int,
    primary key (id)
);

create table user_transaction (
    user_id int, 
    foreign key (user_id) references users(id) on delete cascade,
    id int auto_increment,
    amount int,
    timestamp timestamp
);