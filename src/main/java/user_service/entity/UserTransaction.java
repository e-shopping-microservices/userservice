package user_service.entity;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Table("user_transaction")
public class UserTransaction {
	
	@Id
	private Integer id;
	private Integer userId;
	private LocalDate timestamp;
	private Integer amount;

}
