package user_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;
import user_service.dto.TransactionRequestDTO;
import user_service.dto.TransactionResDTO;
import user_service.dto.TransactionStatus;
import user_service.repository.TransactionRepo;
import user_service.repository.UserRepo;
import user_service.util.EntityDtoUtil;

@Service
public class TransactionService {
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private TransactionRepo transRepo;
	
	
	public Mono<TransactionResDTO> createTransaction(TransactionRequestDTO dto) {
		return userRepo.updateUserBalance(dto.getUserId(), dto.getAmount())
		   .filter(Boolean::booleanValue)
		   .map(e -> EntityDtoUtil.toEntity(dto))
		   .flatMap(this.transRepo::save)
		   .map(trans -> EntityDtoUtil.toDTO(dto, TransactionStatus.APPROVED))
		   .defaultIfEmpty(EntityDtoUtil.toDTO(dto, TransactionStatus.DECLINED));
	}
}
