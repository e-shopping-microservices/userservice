package user_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import user_service.dto.UserDTO;
import user_service.entity.UserTransaction;
import user_service.repository.TransactionRepo;
import user_service.repository.UserRepo;
import user_service.util.EntityDtoUtil;

@Service
public class UserService {

	@Autowired
	private UserRepo repo;
	
	@Autowired
	private TransactionRepo trans_repo;
	
	public Mono<UserDTO> getUser(Integer id) {
		return this.repo.findById(id)
				.map(EntityDtoUtil::toDTO);
	}
	
	public Flux<UserDTO> getAllUser() {
		return this.repo.findAll()
				.map(EntityDtoUtil::toDTO);
	}
	
	public Mono<UserDTO> addUser(Mono<UserDTO> dto) {
		return dto 
		.map(EntityDtoUtil::toEntity)
		.flatMap(user -> this.repo.save(user)
				            .map(EntityDtoUtil::toDTO));
	}
	
	public Mono<UserDTO> updateUser(Mono<UserDTO> dto, Integer id) {
		return dto 
		.map(EntityDtoUtil::toEntity)
		.flatMap(user -> this.repo.findById(id)
				.map(ouser -> {user.setId(ouser.getId()); return user;}))	
		.flatMap(user -> this.repo.save(user))
		.map(EntityDtoUtil::toDTO);
	}
	
	public Mono<Void> deleteUser(Integer id) {
		return this.repo.deleteById(id);
	}
	
	public Mono<Integer> countTransPerUser(int id){
		return this.trans_repo.countUserTransactionByUserId(id);
	}
	
}
