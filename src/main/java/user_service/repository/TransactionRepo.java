package user_service.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import user_service.entity.UserTransaction;

@Repository
public interface TransactionRepo extends ReactiveCrudRepository<UserTransaction, Integer> {
	
	Mono<Integer> countUserTransactionByUserId(int id);
	
}
