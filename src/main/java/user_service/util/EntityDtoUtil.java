package user_service.util;

import java.time.LocalDate;
import java.util.Date;

import user_service.dto.TransactionRequestDTO;
import user_service.dto.TransactionResDTO;
import user_service.dto.TransactionStatus;
import user_service.dto.UserDTO;
import user_service.entity.User;
import user_service.entity.UserTransaction;

public class EntityDtoUtil {
	
	
	public static UserDTO toDTO(User user) {
		UserDTO dto = new UserDTO();
		dto.setBalance(user.getBalance());
		dto.setId(user.getId());
		dto.setName(user.getName());
		return dto;
	}
	
	public static User toEntity(UserDTO dto) {
		User user = new User();
		user.setBalance(dto.getBalance());
		user.setId(dto.getId());
		user.setName(dto.getName());
		return user;
	}
	
	public static TransactionResDTO toDTO(TransactionRequestDTO trans, TransactionStatus status) {
		TransactionResDTO dto = new TransactionResDTO();
		dto.setAmount(trans.getAmount());
		dto.setStatus(status);
		dto.setUserId(trans.getUserId());
		return dto;
	}
	
	public static UserTransaction toEntity(TransactionRequestDTO dto) {
		UserTransaction user = new UserTransaction();
		user.setAmount(dto.getAmount());
		user.setUserId(dto.getUserId());
		user.setTimestamp(LocalDate.now());
		return user;
	}

}
