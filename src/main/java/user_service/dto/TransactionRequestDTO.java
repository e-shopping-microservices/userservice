package user_service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
@AllArgsConstructor
public class TransactionRequestDTO {
	
	private Integer userId;
	private Integer amount;

}
