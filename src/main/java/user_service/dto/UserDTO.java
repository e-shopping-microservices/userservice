package user_service.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class UserDTO {
	
	private Integer id;
	private String name;
	private Integer balance;

}
