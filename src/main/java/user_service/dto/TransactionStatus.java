package user_service.dto;

public enum TransactionStatus {
	
	APPROVED, 
	DECLINED

}
