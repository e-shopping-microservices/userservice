package user_service.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TransactionResDTO {
	
	private Integer userId;
	private Integer amount;
	private TransactionStatus status;

}
