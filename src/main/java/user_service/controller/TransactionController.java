package user_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;
import user_service.dto.TransactionRequestDTO;
import user_service.dto.TransactionResDTO;
import user_service.dto.TransactionStatus;
import user_service.service.TransactionService;

@RestController
@RequestMapping("/user/transaction")
public class TransactionController {

	@Autowired
	private TransactionService service;
	
	@PostMapping
	public Mono<ResponseEntity<TransactionResDTO>> createTrans(@RequestBody TransactionRequestDTO dto) {
		return this.service.createTransaction(dto)
		    .map(res -> {
		    	if(res.getStatus().equals(TransactionStatus.DECLINED)) 
		    		return ResponseEntity.badRequest().body(res);
		    	else return ResponseEntity.ok(res);
		    });
	}
	
}
