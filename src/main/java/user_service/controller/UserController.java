package user_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import user_service.dto.UserDTO;
import user_service.entity.UserTransaction;
import user_service.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService service;
	
	@GetMapping("/all")
	public Flux<UserDTO> getAll() {
		return this.service.getAllUser();
	}
	
	@GetMapping("/transaction/{id}")
	public Mono<Integer> getTransUser(@PathVariable("id") Integer id) {
		return this.service.countTransPerUser(id);
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<UserDTO>> get(@PathVariable("id") Integer id) {
		return this.service.getUser(id)
				.map(ResponseEntity::ok) 
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<UserDTO> add(@RequestBody Mono<UserDTO> dto) {
		return this.service.addUser(dto);
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<UserDTO>> update(@PathVariable("id") Integer id,
			@RequestBody Mono<UserDTO> dto) {
		return this.service.updateUser(dto, id)
				.map(ResponseEntity::ok) 
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public Mono<Void> delete(@PathVariable("id") Integer id) {
		return this.service.deleteUser(id);
	}
}
